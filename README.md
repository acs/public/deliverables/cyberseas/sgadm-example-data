# SGADM-related-information

This repository contains data and information related to the SGADM tool https://git.rwth-aachen.de/acs/public/software/sgadm: 

- Example of assets and dependencies input-excel sheet: "_asset_dep_example.xlsx_". Kindly make use of this excel sheet for providing the asset  information in SGADM tool.
- Presentation of Task 2.1 : "_CyberSEAS_Task2_1.pptx_"
- AHP questionnaire response: "_CyberSEAS_T2_1_Criteria_comparision_Informatika_v01.docx_"
- Weights calculated using AHP technique:  "_CIA_AssetClasses_Comparison_AHP_Informatika.xlsx_"
- User-manual : "_CyberSEAS_Deliverable_2.1.pdf_"
- General list of EPES assets : "_General_List_of_Assets_SGAM_v04.xlsx_"

